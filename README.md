# Setup an Ubuntu-based device

## Prerequisites

### Enable passwordless sudo
On the device:
```
$ sudo visudo 
```
Add `<USERNAME> ALL=(ALL) NOPASSWD:ALL`

### Install SSH server
On the device:
```
$ sudo apt install -y openssh-server
```

### Generate a new SSH key
On your local machine:
```
$ ssh-keygen -f ./keys/liva-z.key
$ scp ./keys/liva-z.pub <USERNAME>@<IP>:/home/<USERNAME>/.ssh/authorized_keys
```

## Installation
```
./startAnsible.sh
```
