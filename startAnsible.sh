#!/usr/bin/env bash

ansible-galaxy install -r requirements.yml
ansible-playbook playbook.yml --key-file keys/liva-z.key
